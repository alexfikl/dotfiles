# Check for an interactive session
[ -z "$PS1" ] && return

# set PS1
if [ -f '/usr/share/git/completion/git-prompt.sh' ]; then
    . /usr/share/git/completion/git-prompt.sh
fi

# set ctrl-w behaviour
stty werase undef
bind '\C-w:unix-filename-rubout'

export GIT_PS1_SHOWDIRTYSTATE=ON
export GIT_PS1_SHOWUNTRACKEDFILES=ON
PS1="\[\e[1;32m\]\u@\h \[\e[1;34m\]\W\[\e[0;33m\]\$(__git_ps1) \[\e[0m\]» "

# import aliases
if [ -f $HOME/.bash_aliases ]; then
    . $HOME/.bash_aliases
fi

# import functions
if [ -f $HOME/.bash_functions ]; then
    . $HOME/.bash_functions
fi

# check if bash-completion is installed etc
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi
complete -cf sudo
complete -cf man

# correct cd mis-spellings
shopt -s cdspell

# more history stuff
shopt -s histappend
PROMPT_COMMAND='history -a'

export HISTFILE="${XDG_STATE_HOME}/bash/history"
export HISTFILESIZE=20000
export HISTSIZE=5000
export HISTCONTROL=ignoredups
export HISTCONTROL=ignoreboth

# exports
if [ -n "$DISPLAY" ]; then
    BROWSER=firefox
else
    BROWSER=lynx
fi

export BROWSER
export EDITOR=vim
export GREP_COLOR="1;33"
export HISTCONTROL="&:[ ]:cd"
export PYTHONSTARTUP="$HOME/.pythonrc"

# colored man pages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

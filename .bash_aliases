# color aliases
eval $(dircolors -b)
alias grep="grep -n -I --color=auto"
alias ls="ls --color=auto"

# prefered command options
alias mkdir="mkdir -p -v"
alias mv="mv -iv"
alias rm="rm -Iv --one-file-system"
alias cp="cp -iv"
alias df="df -h"
alias du="du -c -h -d 1"
alias ping="ping -c 5"
alias gcc="gcc -O -Wall -Werror -Wshadow -Wextra -pedantic -std=c99 \
    -Wfloat-equal -Wundef -Wpointer-arith -Wcast-align -Wstrict-prototypes \
    -Wwrite-strings -Wswitch-default -Wunreachable-code -Wformat=2"

# sudo shortcuts
alias svim="sudo vim"

# others
alias lss='ls -shAxSr'
alias cmakeall="mkdir build && cd build; cmake ..; make"
alias so="source ~/.bashrc"
alias ..='cd ..'
alias ...='cd ../..'

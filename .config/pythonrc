"""Best goddamn .pythonrc file in the whole world."""

import sys

try:
    __IPYTHON__
    sys.exit(0)
except NameError:
    pass

import os
import math  # noqa: F401
import shutil
import pathlib
import readline
import rlcompleter  # noqa: F401
import atexit
from code import InteractiveConsole

AUTHOR = "Alexandru Fikl <alexfikl at gmail dot com>"


# {{{ color support


class TermColors(dict):
    """Gives easy access to ANSI color codes. Attempts to fall back to no color
    for certain TERM values. (Mostly stolen from IPython.)"""

    COLOR_TEMPLATES = (
        ("Black", "0;30"),
        ("Red", "0;31"),
        ("Green", "0;32"),
        ("Brown", "0;33"),
        ("Blue", "0;34"),
        ("Purple", "0;35"),
        ("Cyan", "0;36"),
        ("LightGray", "0;37"),
        ("DarkGray", "1;30"),
        ("LightRed", "1;31"),
        ("LightGreen", "1;32"),
        ("Yellow", "1;33"),
        ("LightBlue", "1;34"),
        ("LightPurple", "1;35"),
        ("LightCyan", "1;36"),
        ("White", "1;37"),
        ("Normal", "0"),
    )

    NoColor = ""
    _base = "\001\033[{c}m\002"

    def __init__(self):
        fmt = ""
        if os.environ.get("TERM") in (
            "xterm-color",
            "xterm",
            "linux",
            "screen",
            "screen-256color",
            "screen-bce",
            "xterm-256color",
            "konsole",
        ):
            fmt = self._base

        self.update({k: fmt.format(c=v) for k, v in self.COLOR_TEMPLATES})


_c = TermColors()

# }}}

# {{{ enable history

# Read the existing history if there is one
HISTFILE = pathlib.Path("~/.pyhistory").expanduser()
if HISTFILE.exists():
    readline.read_history_file(HISTFILE)

# Set maximum number of items that will be written to the history file
readline.set_history_length(10000)
readline.parse_and_bind("tab:complete")


def savehist():
    readline.write_history_file(HISTFILE)


atexit.register(savehist)

# }}}

# {{{ enable color prompts

sys.ps1 = "{}>>> {}".format(_c["Green"], _c["Normal"])
sys.ps2 = "{}... {}".format(_c["Red"], _c["Normal"])

# }}}

# {{{ enable pretty printing for stdout

_RICH_TRY_ENABLED = False


def _enable_rich():
    global _RICH_TRY_ENABLED
    if _RICH_TRY_ENABLED:
        return

    _RICH_TRY_ENABLED = True

    from rich import pretty

    pretty.install()


def _custom_displayhook(value):
    _enable_rich()

    if value is not None:
        try:
            import __builtin__

            __builtin__._ = value
        except ImportError:
            __builtins__._ = value

        from rich.pretty import pprint

        pprint(value)


sys.displayhook = _custom_displayhook

# }}}

# {{{ numpy options

try:
    import numpy as np

    linewidth = shutil.get_terminal_size((85, 20))[0] - 7
    np.set_printoptions(linewidth=linewidth)
except ImportError:
    pass

# }}}

# {{{ utils


def chunks(iterable, step, n=None):
    if n is None:
        n = len(iterable)

    for i in range(0, n, step):
        yield iterable[i : i + step]


def rgb2hex(*rgb) -> str:
    assert len(rgb) == 3

    if not isinstance(rgb[0], int):
        rgb = (int(rgb[0] * 255), int(rgb[1] * 255), int(rgb[2] * 255))

    return f"#{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}".upper()


def hex2rgb(chex: str):
    chex = chex.strip("#")
    assert len(chex) == 6

    return tuple([int(x, 16) for x in chunks(chex, 2)])


# }}}

# {{{ fancy interative buffer

# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/438813/

EDITOR = os.environ.get("EDITOR", "nvim")
EDIT_CMD = r"\e"
SHELL_CMD = "!"
DOC_CMD = "?"


class EditableBufferInteractiveConsole(InteractiveConsole):
    def __init__(self, *args, **kwargs):
        # this holds the last executed statement
        self.last_buffer = []
        InteractiveConsole.__init__(self, *args, **kwargs)

    def runsource(self, source, *args, **kwargs):
        self.last_buffer = [source.encode("utf-8")]
        return super().runsource(source, *args, **kwargs)

    def _process_edit_cmd(self, line):
        import tempfile

        with tempfile.NamedTemporaryFile(suffix=".py", delete=False) as fd:
            fd.write(b"\n".join(self.last_buffer))
            tmpfile = fd.name

        os.system(f"{EDITOR} {tmpfile}")
        lines = open(tmpfile).read().split("\n")
        os.unlink(tmpfile)

        for i in range(len(lines) - 1):
            self.push(lines[i])

        return lines[-1]

    def _process_shell_cmd(self, line):
        def shell_error(msg):
            return "{LightRed}{{}}{Normal}".format(**_c).format(msg)

        import shlex
        import subprocess

        cmd = shlex.split(line[1:])
        if cmd[0] == "cd":
            path = pathlib.Path(" ".join(cmd[1:]) or "~").expanduser()
            try:
                os.chdir(path)
            except Exception as e:
                print(shell_error(f"{e.strerror}: {path}"))
        else:
            process = subprocess.Popen(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            (out, err), _ = (process.communicate(), process.returncode)
            if err:
                print(shell_error(err.decode("utf-8").strip()))
            else:
                print(out.decode("utf-8"))

        return ""

    def _process_doc_cmd(self, line):
        return f"help({line[1:]})"

    def raw_input(self, *args):
        line = InteractiveConsole.raw_input(self, *args)
        if len(line) == 0:
            return line

        if line[:2] == EDIT_CMD:
            return self._process_edit_cmd(line)
        elif line[0] == SHELL_CMD:
            return self._process_shell_cmd(line)
        elif line[0] == DOC_CMD:
            return self._process_doc_cmd(line)

        return line


WELCOME = r"""{Brown}
Type `\e` to get an external editor to write a longer snippet.
Type `?object` to get help about the object.
Type `!shell_command` to execute a shell command.
{Normal}""".format(**_c)

atexit.register(
    lambda: sys.stdout.write(
        """{DarkGray}
See you, space cowboy...
{Normal}""".format(**_c)
    )
)


c = EditableBufferInteractiveConsole(locals=locals())
c.interact(banner=WELCOME)


# }}}

# Exit the Python shell on exiting the InteractiveConsole
sys.exit()

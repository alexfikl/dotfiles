# aliases for XDG spec
alias gdb="gdb -n -x ${XDG_CONFIG_HOME}/gdb/init"
alias mbsync="mbsync -c ${XDG_CONFIG_HOME}/isync/mbsyncrc"

# helpful defaults
alias svim="sudoedit"
alias vim="nvim"
alias vi="nvim"
alias emacs="emacs -mm"
alias locate="plocate"
alias grep="rg"
alias diff="diff --color=auto"
alias feh="feh --scale-down"
alias feha="feh --scale-down --force-aliasing"
alias mpv="mpv --keep-open --autofit=50% --loop"

alias mpirun="mpirun --oversubscribe"
alias octave="octave --no-gui"
alias matlab="matlab -nodisplay -nodesktop"

alias ipa="ip -brief -color addr"
alias ipl="ip -brief -color link"

alias rsyncp="rsync --archive -hh --partial --info=stats1 --info=progress2"
alias dmesg="dmesg -T"

alias pygrind="valgrind --tool=memcheck --leak-check=yes --suppressions=${HOME}/.valgrind/python.supp"

# new commands
alias so="source ${ZDOTDIR:-$HOME}/.zshrc"
alias papers="papis -l papers"
alias books="papis -l books"

# happens to the best of us
alias :q=" exit"
alias cd..="cd .."

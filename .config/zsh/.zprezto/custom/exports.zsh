# pacdiff
export DIFFPROG="nvim -d"

# fzf
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'

# papis
export PAPIS_NP=0

# {{{ Python

# python
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/pythonrc"
export PYTHONBREAKPOINT="pudb.set_trace"

# pytest
export PYTEST_ADDOPTS="-rswx --durations=25"

# }}}

# {{{ OpenCL

# export PYOPENCL_CTX="intel(r)"
# export PYOPENCL_TEST="intel(r)"
export PYOPENCL_CTX="portable"
export PYOPENCL_TEST="portable"

export POCL_MAX_PTHREAD_COUNT=12
export POCL_AFFINITY=1

# }}}

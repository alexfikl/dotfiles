function search() {
  baloosearch "$1" | rg \
    --colors 'match:fg:green' \
    --colors 'match:bg:black' "$1"
}

function matlab-run() {
  matlab -nodisplay -nosplash -nodesktop -r "run('${1}')"
}

function calc() {
  python -c "from math import *; print(eval($*))"
}

function clip() {
  cat "$@" | wl-copy --trim-newline
}

function pwc() {
  echo "$(pwd)" | wl-copy --trim-newline
}

function watchtemp {
    watch -n1 "sensors coretemp-isa-0000"
}

function watchfreq {
    watch -n1 "cat /proc/cpuinfo | grep \"^[c]pu MHz\""
}

function lso() {
    ls -alG "$@" | awk '{k=0;for(i=0;i<=8;i++)k+=((substr($1,i+2,1)~/[rwx]/)*2^(8-i));if(k)printf(" %0o ",k);print}'
}

function weather() {
  local location="${1:-Timisoara}"
  curl "wttr.in/${location}?m&T" | $PAGER
}

function weather2() {
  local location="${1:-Timisoara}"
  curl "v2.wttr.in/${location}?m&T" | $PAGER
}

function hex2rgb() {
  local hex="$1"
  printf "%d %d %d\n" 0x${hex:0:2} 0x${hex:2:2} 0x${hex:4:2}
}

function extract-frame() {
  ffmpeg -i "$1" -vf 'select=eq(n\,0)' -q:v 3 "${1%%.*}.png"
}

function pkg-largest {
  expac -H M '%m\t%n' | sort -h
}

function pkg-latest {
  expac --timefmt='%Y-%m-%d %T' '%l\t%n (%w)' | sort | tail -n 30
}

function convert-a4paper {
  local infile="$1";
  local outfile="${2:-${infile%%.*}.pdf}"
  local tmpfile=$(mktemp --suffix '.pdf')

  convert "${infile}" -background white "${tmpfile}"
  pdfjam --outfile "${outfile}" --paper a4paper "${tmpfile}" 2>/dev/null

  echo "Converted '${infile}' to '${outfile}'"
  rm -rf "${tmpfile}"
}

# vim:set ts=2 sts=2 sw=2 et:

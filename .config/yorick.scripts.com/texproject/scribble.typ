// {{{ formatting

#set text(lang: "en", font: "IM Fell English PRO")

#let title = "Title"
#let authors = ("Alexandru Fikl",)
#let date = "July 6, 2023"

#set document(author: authors, title: title)
#set page(paper: "a4", numbering: "1", number-align: center)
#set par(justify: true)
#set heading(numbering: "1.")

// Title
#align(center)[
  #block(text(weight: 700, 1.75em, title))
  #v(1em, weak: true)
  #date
]

// Author
#pad(
    top: 0.5em,
    bottom: 0.5em,
    x: 2em,
    grid(
      columns: (1fr,) * calc.min(3, authors.len()),
      gutter: 1em,
      ..authors.map(author => align(center, strong(author))),
  ),
)

// }}}

= Introduction

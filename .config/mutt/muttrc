# {{{ basic options

set mbox_type = Maildir
set folder = "/mnt/data/maildir/gmail/"

set realname = "Alexandru Fikl"

set smtp_authenticators = "login"
set imap_authenticators = "login"
set ssl_force_tls = yes

# macro to run mbsync
macro index G "!mbsync -a^M" "Update through mbsync"

# group by threads
set sort = 'threads'
# mail ordering
set sort_aux = 'reverse-last-date-received'
# prefer plain text
alternative_order multipart/mixed multipart/related text/plain
# this line gets rid of iso 8859 issues with attachments
set rfc2047_parameters

# include messages when replying
set include
# don't ask when moving
set move = no
# sync editors
set editor="`echo \$EDITOR`"

# notmuch
set virtual_spoolfile=yes
source ~/.mailboxes

# }}}

# {{{ gmail

set imap_user = "alexfikl@gmail.com"
set imap_pass = "`gopass show --password google.com/alexfikl`"
set from = $imap_user

set smtp_url = "smtp://$imap_user@smtp.gmail.com:587/"
set smtp_pass = $imap_pass

# do not record sent emails since gmail does this automatically
unset record
# set mailboxes
set mbox="+Inbox"
set spoolfile="+Inbox"
mailboxes +Inbox +Sent \
    +'MailingLists/Github' \
    +'America' +'America/Housing' +'America/Finance' +'America/PhD' \
    +'France' +'France/CEMRACS' +'France/Internship' +'France/School' +'France/Residence' \
    +'Misc' +'Misc/Brainpickings' +'Misc/Fermat' +'Misc/Mendeley' +'Misc/Remember' \
    +'Romania' +'Romania/School' +'Romania/Thesis' \
    +'Pen Palls' +'Research' +'Travel'

# alternative emails
alternates "fikl2@illinois.edu"

# }}}

# {{{ appearance

# headers
ignore *                               # first, ignore all headers
unignore from: to: cc: date: subject:  # then, show only these
hdr_order from: to: cc: date: subject: # and in this order

# sort sidebar
set sidebar_sort_method = name
# toggle sidebar
bind index,pager B sidebar-toggle-visible
# traverse sidebar
bind index,pager \CN sidebar-next
bind index,pager \CP sidebar-prev
bind index,pager \CO sidebar-open

# colors
source ~/.mutt/solarized.muttrc

set date_format="%b %d %Y, %R"
set index_format="%4C | %Z [%d] %-30.30F (%-4.4c) %s"

# }}}

augroup vimrc
    autocmd!
augroup END

" {{{ Basic Settings

filetype on                 " try to detect filetypes
set mouse=a                 " enable mouse
set number                  " display line numbers
set numberwidth=1           " using only 1 column (and 1 space) while possible
set background=dark         " we are using dark background in vim
set relativenumber          " display relative line numbers
" set cursorline              " highlight the current line

" {{{ Moving Around / Editing

set ruler                   " show the cursor position all the time
set nostartofline           " avoid moving cursor to BOL when jumping around
set virtualedit=block       " let cursor move past the last char in virtual mode
set scrolloff=3             " keep 3 context lines above and below the cursor
set showmatch               " briefly jump to a parenthesis once it's balanced
set matchtime=2             " (for only .2 seconds).
set tabstop=4               " <tab> inserts 4 spaces
set shiftwidth=4            " indent level is 4 spaces wide.
set softtabstop=4           " <BS> over an autoindent deletes both spaces.
set expandtab               " use spaces, not tabs, for autoindent/tab key.
set shiftround              " rounds indent to a multiple of shiftwidth
set matchpairs+=<:>         " show matching <> (html mainly) as well
set foldmethod=marker       " allow us to fold on specific markers
set foldlevel=99            " don't fold by default
set wildmode=full           " <Tab> cycles between all matching choices.
set pumheight=6             " keep a small completion window
set completeopt=menuone,longest,preview
set wrap linebreak nolist

" }}}

" {{{ Reading / Writing

set noautowrite                 " never write a file unless I request it.
set noautowriteall              " NEVER.
set modeline                    " allow vim options to be embedded in files;
set modelines=5                 " they must be within the first or last 5 lines.
set fileformats=unix,dos,mac    " try recognizing dos, unix, and mac line endings.
set guioptions+=a               " put stuff in the system's clipboard
set clipboard=unnamedplus
set spelloptions+=camel,noplainbuffer       " requires for treesitter spelling

" }}}

" {{{ Messages, Info, Status

set laststatus=2            " always show status line
set visualbell t_vb=        " disable all bells
set confirm                 " Y-N-C prompt if closing with unsaved changes.
set showcmd                 " show incomplete normal mode commands as I type.
set report=0                " : commands always print changed line count.
set shortmess+=a            " use [+]/[RO]/[w] for modified/readonly/written.
set statusline=%<[%n]\ %f\ [%{&ft}]%=%-19(%3l,%02c%03V%)

" }}}

" {{{ Searching and Patterns

set ignorecase              " default to using case insensitive searches,
set smartcase               " unless uppercase letters are used in the regex.
set inccommand=nosplit      " show substitude incrementally while typing

" }}}

" {{{ Sessions

set ssop-=options           " do not save global and local values in sessions
set ssop-=folds             " do not store folds

let g:session_directory = '~/.config/nvim/sessions'
let g:session_autosave = 'yes'

" }}}

" {{{ netrw

let g:netrw_banner=0        " disable annoying banner
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view

" }}}

" }}}

" {{{ Advanced Settings

" improve start up hardcoding the executable
let g:python3_host_prog = "python"
" improve start up by disabling black
let g:load_black = 1

" turn on block moving
runtime macros/matchit.vim

" ignore these files when completing
set wildignore+=.svn,.hg,.git,CVS,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif,*.jar,*.exe,*.bin,*.zip

" highlight trailing whitespace
set list listchars=tab:»\ ,trail:·,nbsp:␣

" auto change the directory to the current file I'm working on
augroup vimrc
    autocmd BufEnter * lcd %:p:h
augroup END

" show a line at column 81
if exists("&colorcolumn")
    set colorcolumn=81
endif

" use inflow (par implementation) for prettier paragraph formatting
set formatprg=inflow\ 88

" print line number and filename when using grep
set grepprg=rg\ -nH\ --vimgrep\ $*

" use just for commands
set makeprg=just

" close preview window automatically when we move around
augroup vimrc
    autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
    autocmd InsertLeave * if pumvisible() == 0|pclose|endif
augroup END

" enable basic completion
augroup vimrc
    autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
    autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
    autocmd FileType css set omnifunc=csscomplete#CompleteCSS
    autocmd FileType python set omnifunc=pythoncomplete#Complete
    set omnifunc=syntaxcomplete#Complete
augroup END

" disable temporary files for password file
augroup vimrc
    autocmd BufRead,BufNewFile /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
augroup END

" recognize files opened from (neo)mutt
augroup vimrc
    autocmd BufRead,BufNewFile *mutt-* setfiletype mail
augroup END

" recognize scons files
augroup vimrc
    autocmd BufRead,BufNewFile SConscript set filetype=python
    autocmd BufRead,BufNewFile SConstruct set filetype=python
augroup END

" disable column numbers in the terminal
function! s:EnableTerminal()
    setlocal nonumber
    setlocal norelativenumber
    setlocal signcolumn=no
endfunction

augroup vimrc
    autocmd TermOpen * call <SID>EnableTerminal()
augroup END

" enable backup
set backup

" set backup directory
let g:backupdir = expand(stdpath("data") . "/backup")
if !isdirectory(g:backupdir)
   mkdirs(g:backupdir, "p")
endif
let &backupdir=g:backupdir

" use fancy diff algorithm
set diffopt+=indent-heuristic,linematch:50

" set float background
highlight NormalFloat guibg=none

" preserve last editing position
" https://stackoverflow.com/a/14449484
augroup vimrc
    autocmd BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
augroup END

" resize splits when the window is resized
augroup vimrc
    autocmd VimResized * exe "normal! \<c-w>="
augroup END

" }}}

" {{{ Shortcuts

let mapleader=","           " change the leader to be a comma vs slash
let maplocalleader=","      " change this one as well

" brings up vimrc
map <leader>ev :sp ~/.config/nvim/init.vim<CR><C-W>_
" reloads vimrc - making all changes active (have to save first)
map <silent> <leader>rv :source ~/.config/nvim/init.vim<CR>:filetype detect<CR>:exe ":echo 'vim configuration reloaded'"<CR>

" remap W to w (common typo)
command! W :w

" for when we forget to use sudo to open/edit a file
cmap w!! w !sudo tee % >/dev/null

" ctrl-jklh changes to that split
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l
nnoremap <c-h> <c-w>h

" navigate buffers
noremap [f :bprev<CR>
noremap ]f :bnext<CR>

" delete current buffer and go back to previous opened one
command! Bd execute "b#|bd#"

" handy command for inserting newlines without insert mode
nnoremap <CR> o<Esc>k

" move by virtual lines when not jumping
nnoremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
nnoremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" make help always vertical
cabbrev help vert help

" center when going to end of file
nnoremap G Gzz

" always use magic mode
nnoremap / /\v

" add a little fancy placeholder and a command to go to the next one
nnoremap ,, :keepp /<++><CR>ca<

" }}}

" {{{ Plugins

" {{{ vim-plug

function! GetGitDir(fallback)
    " get git directory or return fallback if not in a git repo
    let root = split(system('git rev-parse --show-toplevel'), '\n')[0]
    return v:shell_error ? fallback : root
endfunction

function! IsGitDir()
    " check if in a git repo
    let dummy = "X-GIT-NOT-FOUND-X"
    return GetGitDir(dummy) == dummy
endfunction

function! PlugLoaded(name)
    return (
        \ has_key(g:plugs, a:name)
        \ && isdirectory(g:plugs[a:name].dir))
endfunction

call plug#begin()

" Plug 'dstein64/vim-startuptime'
Plug 'sainnhe/sonokai'

" filetypes
Plug 'NoahTheDuke/vim-just'
Plug 'JuliaEditorSupport/julia-vim', { 'for': 'julia' }
Plug 'chunkhang/vim-mbsync'
Plug 'ziglang/zig.vim', { 'for': 'zig' }
Plug 'raimon49/requirements.txt.vim'

" text objects
Plug 'wellle/targets.vim'

" ui
Plug 'ap/vim-buftabline'
Plug 'ibhagwan/fzf-lua'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'akinsho/git-conflict.nvim'

" tpope
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'

" linting
Plug 'dense-analysis/ale'

" syntax
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'

call plug#end()

" }}}

" {{{ Plugin Configuration

" {{{ editorconfig-vim

lua << EOF
    vim.api.nvim_create_autocmd("BufWritePre", {
        pattern = "*",
        group = "vimrc",
        callback = function ()
            local uv = vim.loop
            local dir = vim.fn.getcwd()
            local has_editorconfig = false

            while true do
                local path = uv.fs_realpath(dir .. "/.editorconfig")
                if path and uv.fs_stat(path) then
                    has_editorconfig = true
                    break
                end

                local parent = uv.fs_realpath(dir .. "/..")
                if parent == dir then
                    break
                end

                dir = parent
            end

            if not has_editorconfig then
                local view = vim.fn.winsaveview()
                vim.api.nvim_command('silent! undojoin')
                vim.api.nvim_command('silent keepjumps keeppatterns %s/\\s\\+$//e')
                vim.fn.winrestview(view)
            end
        end,
    })
EOF

" }}}

" {{{ colorscheme

if PlugLoaded("sonokai")
    set termguicolors
    let g:sonokai_style = 'default'
    let g:sonokai_better_performance = 1

    colorscheme sonokai
else
    if has("nvim-0.10")
        " NOTE: the color scheme changed in neovim 0.10, so this keeps it the same
        highlight Comment guifg=#80a0ff ctermfg=Cyan
        set notermguicolors
    endif
endif

" }}}

" {{{ julia-vim

if PlugLoaded("julia-vim")
    " do not use latex by default
    let g:latex_to_unicode_auto = 0
endif

" }}}

" {{{ zig.vim

if PlugLoaded("zig.vim")
    " toggle formatting with zig-fmt
    let g:zig_fmt_autosave = 1
endif

" }}}

" {{{ vimtex

if PlugLoaded("vimtex")
    let g:vimtex_enabled = 1

    " enable basic completion
    let g:vimtex_complete_bib = { 'simple': 1 }
    let g:vimtex_parser_bib_backend = 'bibtexparser'

    " only show on errors
    let g:vimtex_quickfix_mode = 2
    let g:vimtex_quickfix_autoclose_after_keystrokes = 1
    let g:vimtex_quickfix_open_on_warning = 0

    " use okular
    let g:vimtex_view_general_viewer = 'okular'
    let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
endif

" }}}

" {{{ vim-buftabline

if PlugLoaded("vim-buftabline")
    " number tabs by the buffer number for easy switching
    let g:buftabline_numbers = 1

    highlight link BufTabLineCurrent StatusLine
endif

" }}}

" {{{ fzf-lua

if PlugLoaded("fzf-lua")
lua << EOF
    require("fzf-lua").setup({
        "fzf-vim",
    })
EOF

    map <leader>t :FzfLua live_grep<CR>
    nnoremap <expr> <leader>f IsGitDir() ? ":FzfLua files<CR>" : ":FzfLua git_files<CR>"
    nnoremap <leader>b :FzfLua buffers<CR>
endif

" }}}

" {{{ indent-blankline.nvim

if PlugLoaded("indent-blankline.nvim")
    " set custom hightlight
    highlight IblIndent ctermfg=239 cterm=nocombine

lua << EOF
    require("ibl").setup({
        indent = { char = '¦' },
        exclude = { buftypes = { "help", "terminal" } },
        scope = { enabled = true, show_start = false, show_end = false },
        whitespace = { remove_blankline_trail = false },
    })
EOF
endif

" }}}

" {{{ vim-tpope

if PlugLoaded("vim-commentary")
    augroup vimrc
        autocmd FileType matlab setlocal commentstring=%\ %s
        " julia-vim sets this to be `#= %s =#` which is a bit weird
        autocmd FileType julia setlocal commentstring=#\ %s
        autocmd FileType c,cpp setlocal commentstring=//\ %s
    augroup END
endif

" }}}

" {{{ ale

if PlugLoaded("ale")
    " only use ruff/flake8, since everything else is slow
    let g:ale_linters = {
        \ 'python': ['ruff', 'flake8'],
        \ 'pyopencl': ['ruff', 'flake8'],
        \ 'rust': ['cargo'],
        \ }
    let g:ale_linter_aliases = {'pyopencl': 'python'}
    let g:ale_rust_cargo_use_clippy = 1

    " make ale less intrusive
    let g:ale_lint_on_text_changed = 'never'
    let g:ale_lint_on_insert_leave = 0
    let g:ale_linters_explicit = 1
    let g:ale_disable_lsp = 1

    " use cleaner message
    let g:ale_echo_msg_format = '[%linter%] %s (%code%) [%severity%]'
    let g:ale_use_neovim_diagnostics_api = 1

    " cycle through errors shortcuts
    nnoremap ]a :ALENextWrap<CR>
    nnoremap [a :ALEPreviousWrap<CR>
endif

" }}}

" {{{ nvim-treesitter

if PlugLoaded("nvim-treesitter")
lua <<EOF
    require'nvim-treesitter.configs'.setup {
        ensure_installed = {
            'bibtex',
            'cmake',
            'comment',
            'typst',
            'yaml',
            'zig',
            'rust',
        },
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = { 'python', 'pyopencl', 'markdown' },
        },
        textobjects = {
            select = {
                enable = true,
                lookahead = true,
                keymaps = {
                    ['af'] = '@function.outer',
                    ['if'] = '@function.inner',
                },
            },
            swap = {
                enable = true,
                swap_next = {
                    ['<leader>xn'] = '@parameter.inner',
                },
                swap_previous = {
                    ['<leader>xp'] = '@parameter.inner',
                },
            }
        }
    }

    -- pyopencl is just an extension of python with some extra syntax
    vim.treesitter.language.register('python', 'pyopencl')
EOF

    " https://github.com/nvim-treesitter/nvim-treesitter/issues/1563
    highlight link @keyword Keyword
endif

" }}}

" {{{ git-conflict.nvim

if PlugLoaded("git-conflict.nvim")
lua <<EOF
    require('git-conflict').setup({
        default_mappings = true
    })
EOF
endif

" }}}

" {{{ nvim-snippy

if PlugLoaded("nvim-snippy")
lua <<EOF
    require('snippy').setup({
        mappings = {
            is = {
                ['<Tab>'] = 'expand_or_advance',
                ['<S-Tab>'] = 'previous',
            },
            nx = {
                ['<leader>x'] = 'cut_text',
            },
        },
    })
EOF
endif

" }}}

" }}}

" }}}

" vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:

syn case match

" PETSc types in Fortran
" PETSc Sys
syn match fortranType           "^\s*PetscEnum\>"
syn match fortranType           "^\s*PetscBool\>"
syn match fortranType           "^\s*PetscInt\>"
syn match fortranType           "^\s*PetscMPIInt\>"
syn match fortranType           "^\s*PetscReal\>"
syn match fortranType           "^\s*PetscScalar\>"
syn match fortranType           "^\s*PetscFortranComplex\>"
syn match fortranType           "^\s*PetscErrorCode\>"
syn match fortranType           "^\s*InsertMode\>"
syn match fortranType           "^\s*PetscObject\>"
syn match fortranType           "^\s*PetscRandom\>"
syn match fortranType           "^\s*PetscRandomType\>"
syn match fortranType           "^\s*PetscDataType\>"

syn keyword fortranConstant     PETSC_i
syn keyword fortranConstant     ADD_VALUES INSERT_VALUES MAX_VALUES
syn keyword fortranConstant     PETSC_COMM_WORLD PETSC_COMM_SELF
syn keyword fortranConstant     PETSC_DECIDE PETSC_DETERMINE
syn keyword fortranConstant     PETSC_FALSE PETSC_TRUE
syn keyword fortranConstant     PETSC_NULL_CHARACTER PETSC_NULL_INTEGER PETSC_NULL_SCALAR PETSC_NULL_DOUBLE PETSC_NULL_REAL PETSC_NULL_OBJECT PETSC_NULL_BOOL PETSC_NULL_FUNCTION
syn keyword fortranConstant     PETSC_NULL_OPTIONS PETSC_NULL_DM PETSC_NULL_VEC PETSC_NULL_MAT PETSC_NULL_KSP PETSC_NULL_SNES PETSC_NULL_PC
syn keyword fortranConstant     PETSC_DEFAULT_INTEGER PETSC_DEFAULT_REAL
syn keyword fortranConstant     PETSC_PI PETSC_MAX_REAL PETSC_MIN_REAL PETSC_MACHINE_EPSILON PETSC_SQRT_MACHINE_EPSILON PETSC_SMALL PETSC_INFINITY PETSC_NINFINITY
syn keyword fortranConstant     PETSCRAND PETSCRAND48 PETSCSPRNG PETSCRANDER48
syn keyword fortranConstant     PETSC_INT PETSC_DOUBLE PETSC_COMPLEX PETSC_LONG PETSC_SHORT PETSC_FLOAT PETSC_CHAR PETSC_BIT_LOGICAL PETSC_ENUM PETSC_BOOL

" PETSc Viewer
syn match fortranType           "^\s*PetscViewer\>"
syn match fortranType           "^\s*PetscViewerFormat\>"
syn match fortranType           "^\s*PetscViewerType\>"
syn match fortranType           "^\s*PetscFileMode\>"

syn keyword fortranConstant     FILE_MODE_WRITE FILE_MODE_READ FILE_MODE_APPEND
syn keyword fortranConstant     PETSCVIEWERSOCKET PETSCVIEWERASCII PETSCVIEWERBINARY PETSCVIEWERSTRING PETSCVIEWERDRAW PETSCVIEWERVU PETSCVIEWERMATHEMATICA PETSCVIEWERNETCDF PETSCVIEWERHDF5 PETSCVIEWERVTK PETSCVIEWERMATLAB
syn keyword fortranConstant     PETSC_VIEWER_STDOUT_SELF PETSC_VIEWER_STDOUT_WORLD
syn keyword fortranConstant     PETSC_VIEWER_DRAW_SELF PETSC_VIEWER_DRAW_WORLD

" PETSc Vec
syn match fortranType           "^\s*Vec\>"
syn match fortranType           "^\s*VecScatter\>"
syn match fortranType           "^\s*VecType\>"
syn match fortranType           "^\s*NormType\>"
syn match fortranType           "^\s*ScatterMode\>"

syn keyword fortranConstant     VECSEQ VECMPI VECSTANDARD VECSHARED VECSEQCUSP VECMPICUSP VECCUSP VECSEQVIENNACL VECMPIVIENNACL VECVIENNACL VECSEQCUDA VECMPICUDA VECCUDA VECNEST
syn keyword fortranConstant     NORM_1 NORM_1_AND_2 NORM_2 NORM_FROBENIUS NORM_INFINITY NORM_MAX
syn keyword fortranConstant     SCATTER_FORWARD SCATTER_REVERSE

" PETSc Mat
syn match fortranType           "^\s*Mat\>"
syn match fortranType           "^\s*MatInfo\>"
syn match fortranType           "^\s*MatNullSpace\>"
syn match fortranType           "^\s*MatCoarsen\>"
syn match fortranType           "^\s*MatCoarsenType\>"
syn match fortranType           "^\s*MatColoring\>"
syn match fortranType           "^\s*MatColoringType\>"

syn keyword fortranConstant     MATAIJ MATAIJCRL MATBAIJ MATDENSE MATMPIADJ MATMPIAIJ MATMPIBAIJ MATMPISBAIJ MATSBAIJ MATSEQAIJ MATSEQBAIJ MATSEQDENSE MATNEST MATCOMPOSITE MATSHELL
syn keyword fortranConstant     MAT_INFO_SIZE MAT_LOCAL MAT_GLOBAL_MAX MAT_GLOBAL_SUM
syn keyword fortranConstant     MAT_INFO_BLOCK_SIZE MAT_INFO_NZ_ALLOCATED MAT_INFO_NZ_USED MAT_INFO_NZ_UNNEEDED MAT_INFO_MEMORY MAT_INFO_ASSEMBLIES MAT_INFO_MALLOCS MAT_INFO_FILL_RATIO_GIVEN MAT_RATIO_FILL_RATIO_NEEDED MAT_INFO_FACTOR_MALLOCS

" PETSc DM
syn match fortranType           "^\s*DM\>"
syn match fortranType           "^\s*DMType\>"
syn match fortranType           "^\s*PetscDS\>"
syn match fortranType           "^\s*PetscDSType\>"
syn match fortranType           "^\s*PetscFE\>"
syn match fortranType           "^\s*PetscFEType\>"
syn match fortranType           "^\s*PetscFV\>"
syn match fortranType           "^\s*PetscFVType\>"
syn match fortranType           "^\s*DMBoundaryType\>"
syn match fortranType           "^\s*DMDAElementType\>"
syn match fortranType           "^\s*DMDAStencilType\>"
syn match fortranType           "^\s*DMInterpolationType\>"

syn keyword fortranConstant     DM_BOUNDARY_NONE DM_BOUNDARY_GHOSTED DM_BOUNDARY_MIRROR DM_BOUNDARY_PERIODIC DM_BOUNDARY_TWIST
syn keyword fortranConstant     DMDA_STENCIL_BOX DMDA_STENCIL_STAR
syn keyword fortranConstant     DMDA DMCOMPOSITE DMSLICED DMSHELL DMPLEX DMCARTESIAN DMREDUNDANT DMPATCH DMMOAB DMNETWORK DMFOREST
syn keyword fortranConstant     DMDA_Q0 DMDA_Q1

" PETSc PC
syn match fortranType           "^\s*PC\>"
syn match fortranType           "^\s*PCType\>"
syn match fortranType           "^\s*PCSide\>"
syn match fortranType           "^\s*PCASMType\>"
syn match fortranType           "^\s*PCGASMType\>"
syn match fortranType           "^\s*PCCompositeType\>"

syn keyword fortranConstant     PCNONE PCJACOBI PCSOR PCLU PCSHELL PCBJACOBI PCMG PCILU PCICC PCASM PCGASM PCKSP PCCOMPOSITE PCREDUNDANT PCNN PCCHOLESKY PCPBJACOBI PCMAT PCFIELDSPLIT PCSVD PCGAMG PCBDDC PCTELESCOPE

" PETSc KSP
syn match fortranType           "^\s*KSP\>"
syn match fortranType           "^\s*KSPType\>"
syn match fortranType           "^\s*KSPNormType\>"
syn match fortranType           "^\s*KSPConvergedReason\>"

syn keyword fortranConstant     KSPRICHARDSON KSPCHEBYSHEV KSPCG KSPPIPECG KSPFCG KSPGMRES KSPBCGS KSPIBCGS KSPCGS KSPLSQR KSPPREONLY KSPBICG KSPMINRES KSPSYMMLQ KSPGCR
syn keyword fortranConstant     KSP_NORM_DEFAULT KSP_NORM_NONE KSP_NORM_PRECONDITIONED KSP_NORM_UNPRECONDITIONED KSP_NORM_NATURAL
syn keyword fortranConstant     KSP_CONVERGED_ATOL KSP_CONVERGED_ITERATING KSP_CONVERGED_ITS KSP_CONVERGED_RTOL
syn keyword fortranConstant     KSP_DIVERGED_BREAKDOWN KSP_DIVERGED_BREAKDOWN_BICG KSP_DIVERGED_RTOL KSP_DIVERGED_INTEFINITE_PC KSP_DIVERGED_ITS KSP_DIVERGED_NONSYMMETRIC

" PETSC Tao
syn match fortranType           "^\s*Tao\>"
syn match fortranType           "^\s*TaoType\>"
syn match fortranType           "^\s*TaoConvergedReason\>"

syn keyword fortranConstant     TAOLMVM TAONLS TAONTR TAONTL TAOCG TAOTRON TAOOWLQN TAOBMRM TAOBLMVM TAOBQPIP TAOGPCG TAONM TAOPOUNDERS TAOLCL TAOSSILS TAOSSFLS TAOASILS TAOASFLS TAOIPM

syn case ignore

" only using free-form Fortran
let g:fortran_free_source=1

" all dos have enddo
let g:fortran_do_enddo=1

" be as strict as possible
let g:fortran_more_precise=1


" most used tex
let g:tex_flavor = "latex"

" always do spellchecking in tex documents
setlocal spell
" but not in comments
let g:tex_comment_nospell = 1

" pick better fold markers
setlocal foldmarker=<<<,>>>

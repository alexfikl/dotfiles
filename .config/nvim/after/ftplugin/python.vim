" http://www.vex.net/~x/python_and_vim.htm
" Some of these settings are already present in the default python.vim

" formatting
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal smarttab
setlocal expandtab
setlocal formatoptions=croql
setlocal autoindent

" error format
set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m

" FIXME: this is required to reset the highlighting when using nvim-treesitter
" together with `additional_vim_regex_highlighting`, which seems to really
" confuse everyone
highlight default pythonTodo ctermfg=Red

" less indent after an open paren (default '&sw * 2')
let g:pyindent_continue = '&sw'
let g:pyindent_open_paren = '&sw'
let g:pyindent_nested_paren = '&sw'
